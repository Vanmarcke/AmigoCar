class ReservationsController < ApplicationController
  before_action :correct_user,   only: [:destroy]

  def new
  end

  def index
  end


  def show
    @reservation = Reservation.find(params[:id])
    @transaction = Transaction.find(@reservation.transaction_id)
  end

  def create
    @user = current_user
    passengers = params[:params][:seats_selected]
    @transaction = Transaction.find(params[:transaction_id])
    @reservation = Reservation.new(transaction_id: @transaction.id, user_id: @user.id)
    if (@transaction.nb_free_seats) && @reservation.save!
      flash[:success] = 'Your reservation was confirmed!'
      @transaction.update_attributes!(nb_reservations: (@transaction.nb_reservations + Integer(passengers)),
                                      nb_free_seats: (@transaction.nb_free_seats - Integer(passengers)))
      redirect_to reservation_url(@reservation)
    else
      flash[:danger] = 'Your reservation failed!'
      redirect_to transaction_url(params[:transaction_id])
    end
  end

  def destroy
    @reservation = Reservation.find(params[:id])
    @transaction = Transaction.find(@reservation.transaction_id)
    if @reservation.destroy!
      @transaction.update_attributes!(nb_reservations: (@transaction.nb_reservations - 1),
                                      nb_free_seats: (@transaction.nb_free_seats + 1))
      flash[:success] = "Reservation deleted"
      redirect_to transaction_url(@transaction)
    else
      flash[:danger] = "Failure ! Reservation was not deleted"
      redirect_to transaction_url(@transaction)
    end
  end

  private

  # Confirms the correct user.
  def correct_user
    @reservation = Reservation.find(params[:id])
    @user = User.find(@reservation.user_id)
    unless current_user?(@user)
      flash[:danger] = "Unauthorized action !"
      redirect_to(root_url)
    end
  end
end
