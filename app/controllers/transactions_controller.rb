class TransactionsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]

  def new
    @user = current_user
    @transaction = Transaction.new
  end

  def index
    @user = current_user
    @transactions = Transaction.paginate(page: params[:page], :per_page => 15)
    if !params[:start_point].blank? && !params[:end_point].blank?
      @transactions = Transaction.search params[:start_point],
                                         where: {arrival_city: params[:end_point]},
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:dep_city]
    elsif !params[:start_point].blank? && params[:end_point].blank?
      @transactions = Transaction.search params[:start_point],
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:dep_city]
    elsif params[:start_point].blank? && !params[:end_point].blank?
      @transactions = Transaction.search params[:end_point],
                                         page: params[:page],
                                         per_page: 15,
                                         fields: [:arrival_city]
    end
  end

  def show
    @user = current_user
    @transaction = Transaction.find(params[:id])
    @reservations = Reservation.where({ :transaction_id => @transaction.id }).all
  end

  def create
    @user = current_user
    @transaction = Transaction.new(transaction_params)
    if @transaction.save
      flash[:success] = "Success!"
      redirect_to transaction_url(@transaction)
    else
      flash[:error] = "Failure?!"
      render 'new'
    end
  end

  def edit
    @user = current_user
    @transaction = Transaction.find(params[:id])
  end

  def update
    @transaction = Transaction.find(params[:id])
    if @transaction.update_attributes(transaction_params)
      flash[:success] = "Travel updated"
      redirect_to @transaction
    else
      render 'edit'
    end
  end

  private

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # Confirms the correct user.
  def correct_user
    @transaction = Transaction.find(params[:id])
    @user = User.find(@transaction.user_id)
    unless current_user?(@user)
      flash[:danger] = "Unauthorized action !"
      redirect_to(root_url)
    end
  end

  def transaction_params
    params.require(:transaction).permit(:dep_city,:arrival_city,:nb_free_seats,
                                        :travel_date,:price,:user_id, :nb_reservations,
                                        :luggage,:smoking,:animals,:car_type,:cooling)
  end

end
