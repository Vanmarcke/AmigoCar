class Reservation < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :transaction_id, presence: true

end
