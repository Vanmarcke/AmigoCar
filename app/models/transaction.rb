class Transaction < ApplicationRecord
  searchkick
  belongs_to :user
  # has_many :reservations
  default_scope -> { order(:travel_date) }
  validates :user_id, presence: true
  validates :dep_city, presence: true
  validates :arrival_city, presence: true
  validates :travel_date, presence: true
  validates :price, presence: true
  validates :nb_reservations, presence: true
  validates :nb_free_seats, presence: true
  validates :car_type, presence:true

end
