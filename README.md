# AmigoCar Project

The goal of this projet is to create a website like https://www.amigoexpress.com/ for the "distributed systems" course given at UQAC.
We choosed Ruby-on-Rails framework. 

## Project Info

* Ruby version : 2.3.3

* Rails version : 5.1.4

* Services :

	- ElasticSearch v5.6.4 with : [Searchkick](https://github.com/ankane/searchkick "")   
	
## Installation instructions  
 
-	Install and configure Ruby, Rails and ElasticSearch 
-	In Gemfile, comment line with bcrypt gem
-	`bundle install`
-	In Gemfile, uncomment line with bcrypt gem
-	`gem install bcrypt --platform=ruby`
-	`rails db:setup` with `RAILS_ENV=production` if you want the production mod (development otherwise)
-	Additional steps for production environnement (see 'Production Environnement' below)
-   `rake searchkick:reindex:all`
- 	`rails server` with `-e production` if you want the production mod

	
#### Production Environnement

- generate a SECRET_KEY_BASE with `rake RAILS_ENV=production secret`
- copy the result key
- configure the variable environnement 'SECRET_KEY_BASE' with the precedent result key as value
- load db if not already done `rails RAILS_ENV=production db:schema:load`
- precompile assets `rails RAILS_ENV=production assets:precompile` or set `config.assets.compile = false` to true in config\environments\production.rb (NOT TESTED !)
- set `config.force_ssl = true` to false to disable SSL in case of error (WARNING : plain http is not recommended...) 
- FINAL STEP : run `rails s -e production`


