require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michel)
  end

  test "unsuccessful edit" do
    #log_in_as(@user)
    post login_path, params: { session: { email:    @user.email,
                                          password: 'mypassword' } }
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "foo@invalid",
                                              password:              "foo",
                                              password_confirmation: "bar" } }

    assert_template 'users/edit'
  end


  test "successful edit" do
    #log_in_as(@user)
    post login_path, params: { session: { email:    @user.email,
                                          password: 'mypassword' } }
    assert is_logged_in?
    get edit_user_path(@user)
    assert_template 'users/edit'
    name  = "FooBar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end

end
