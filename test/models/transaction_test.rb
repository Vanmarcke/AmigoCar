require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  def setup
    @user = users(:michel)
    @transaction = @user.transactions.build(dep_city: 'Quebec',
                                           arrival_city: 'Montreal',
                                           travel_date: '2017-11-08 15:33:23',
                                           price: 1,
                                           nb_reservations: 1,
                                           nb_free_seats: 1,
                                            car_type:"Berline",
                                            cooling:false,
                                            animals:true,
                                            smoking:false,
                                            luggage:true


    )
  end

  test 'should be valid' do
    assert @transaction.valid?
  end

  test 'user id should be present' do
    @transaction.user_id = nil
    assert_not @transaction.valid?
  end

  test 'price should be present' do
    @transaction.price = nil
    assert_not @transaction.valid?
  end

  test "order should be earliest trip first" do
    assert_equal transactions(:most_recent), Transaction.first
  end
end
