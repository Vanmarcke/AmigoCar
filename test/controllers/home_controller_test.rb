require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "AmigoCar"
  end

  test "should get search" do
    get home_url
    assert_response :success
    assert_select "title", "#{@base_title} | Search"
  end

  test "should get help" do
    get help_url
    assert_response :success
    assert_select "title", "#{@base_title} | Help"
  end

  test "should get about" do
    get about_url
    assert_response :success
    assert_select "title", "#{@base_title} | About"
  end

  test "should get contact" do
    get contact_url
    assert_response :success
    assert_select "title", "#{@base_title} | Contact"
  end

end
