require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michel)
    @other_user = users(:jeanjacques)
  end


  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end


  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end


  test "should redirect edit when logged in as wrong user" do
    post login_path, params: { session: { email:    @other_user.email,
                                          password: 'mypassword' } }
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    post login_path, params: { session: { email:    @other_user.email,
                                          password: 'mypassword' } }
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  # admin test
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    post login_path, params: { session: { email:    @other_user.email,
                                          password: 'mypassword' } }

    assert_no_difference 'User.count' do
      delete user_path(@user)
    end

    assert_redirected_to root_url
  end

  test "index as admin including pagination and delete links" do
    post login_path, params: { session: { email:    @user.email,
                                          password: 'mypassword' } }
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.first_name + " " + user.name
      unless user == @user
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@other_user)
    end
  end

  test "index as non-admin" do
    post login_path, params: { session: { email:    @other_user.email,
                                          password: 'mypassword' } }
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end


end
