# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Vanmarcke",
             first_name: "Romain",
             telephone: "012-345-6789",
             email: "romain@amigo.com",
             password:              "myfoobar",
             password_confirmation: "myfoobar",
              admin: true )

99.times do |n|
  name  = Faker::Name.unique.last_name
  first_name = Faker::Name.first_name
  email = "example-#{n+1}@amigo.com"
  password = "password"
  User.create!(name:  name.delete(' ').delete('\''),
               telephone:"000-000-0000",
               first_name: first_name,
               email: email,
               password:              password,
               password_confirmation: password)
end


@Ville = {"Montreal"=>"Montreal", "NewYork"=>"New-York",
          "Toronto"=> "Toronto", "LosAngeles"=>"Los Angeles",
          "Miami"=>"Miami", "Saguenay"=>"Saguenay",
          "Ottawa"=>"Ottawa", "Vancouver"=>"Vancouver",
          "Houston"=>"Houston",
          "Baltimore"=>"Baltimore", "Quebec"=>"Quebec"}


Transaction.create!(user_id:1,
    dep_city: @Ville["Ottawa"],
    arrival_city: @Ville["Houston"],
    travel_date:  "2017-11-11" ,
    price:20,
    nb_reservations:0,
    nb_free_seats: 2,
    luggage:true,
    animals:true,
    smoking:true,
    car_type:"Berline",
    cooling:true


)



74.times do|n|
  id = rand(User.all.count)
  cities = @Ville.to_a.sample(2)
  dep_city= @Ville[cities[0][0]]
  arrival_city = @Ville[cities[1][0]]

  Transaction.create(user_id:id,
                     dep_city:dep_city,
                     arrival_city: arrival_city,
                     travel_date: Faker::Date.forward(30),
                     price: rand(10..40),
                     nb_reservations:0,
                     nb_free_seats: rand(1..3),
                     luggage:true,
                     animals:true,
                     smoking:true,
                     car_type:"Berline",
                     cooling:true

  )

end