class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.references :user, foreign_key: true
      t.string :dep_city
      t.string :arrival_city
      t.datetime :travel_date
      t.integer :price
      t.integer :nb_reservations
      t.integer :nb_free_seats

      t.timestamps
    end
    add_index :transactions, [:user_id, :created_at]
  end
end
