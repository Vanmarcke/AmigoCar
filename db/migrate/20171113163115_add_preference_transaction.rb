class AddPreferenceTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :smoking, :boolean, default:false
    add_column :transactions, :animals, :boolean, default:false
    add_column :transactions, :luggage, :boolean, default:false
    add_column :transactions, :car_type, :string
    add_column :transactions, :cooling, :boolean, default:false
end
end
