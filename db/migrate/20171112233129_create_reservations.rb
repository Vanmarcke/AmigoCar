class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.references :transaction, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :reservations, [:user_id, :transaction_id], unique: true
  end
end
