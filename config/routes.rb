Rails.application.routes.draw do
  root 'home#search'

  get '/home', to: 'home#search'
  get '/help', to: 'home#help'
  get '/about',to: 'home#about'
  get '/contact', to: 'home#contact'

  get '/login',to: 'sessions#new'
  post '/login',to: 'sessions#create'
  delete '/logout',to: 'sessions#destroy'

  #resource
  resources :users
  resources :transactions
  resources :reservations

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


end
